// Functions Demo
// Paul Haller

#include <iostream>
#include <conio.h>

using namespace std;

// function prototype
// & means pass "by ref"

void SayHi(int &count, bool proper = false);
int Fib(int index);

// an example of passing by "by value"
int AddOneto(int number) { return number + 1; }

// function overloading
// Method -- Function that is inside of a class

// recursion
// A function that calls itself...until it doesn't
void CountDownFrom(int number);


int main()
{
	// SayHi(AddOneto(3));

	//int j = 3;
	//cout << j << "\n";
	//SayHi(j);
	//cout << j << "\n";

	cout << Fib(10);

	(void)_getch();
	return 0;
}



void SayHi(int &count, bool proper)
{
	for (int i = 0; i < count; i++)
	{
		if (proper) cout << "Hello\n";
		else cout << "Hi\n";

		count = 100;
	}
}

void CountDownFrom(int number)
{
	if (number >= 0)
	{
		std::cout << number << "\n";
		CountDownFrom(number - 1);
	}
}

int Fib(int index)
{
	if (index <= 2) return 1;
	return Fib(index - 1) + Fib(index - 2);
}

